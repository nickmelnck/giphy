//
//  AppCoordinator.swift
//
//  Created by Nick Melnick on 12.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import UIKit
import GiphyUISDK
import GiphyCoreSDK

class AppCoordinator: BaseCoordinator {
    
    private var window: UIWindow!
        
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
        Giphy.configure(apiKey: "ZbykVW2l2IFOjGrTVGFY5Q8T2FCsOf2b", verificationMode: false)
    }
    
    override func start() {
        self.window.makeKeyAndVisible()
        showGifListScene()
    }
    
    func showGifListScene() {
        self.removeChildCoordinators()
        
        let coordinator = GifListCoordinator()
        coordinator.navigationController = BaseNavigationController()
        self.start(coordinator: coordinator)
        
        ViewControllerUtils.setRootViewController(
            window: self.window,
            viewController: coordinator.navigationController,
            withAnimation: true
        )
    }
    
}
