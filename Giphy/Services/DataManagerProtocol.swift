//
//  DataManagerProtocol.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import RxSwift

protocol DataManagerProtocol {
    
    func getGifs(_ searchText: String, limit: Int, offset: Int) -> Observable<[GifImage]>
    
}
