//
//  DataManager.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Moya
import RxSwift
import RxCodable
import Foundation

enum APIError: String, Error {
    case unableToComplete = "Unable complete your request. Please check your internet connection"
    case invalidResponse = "Invalid response from the server. Please try again."
    case invalidData = "The data received from the server was invalid. Please try again."
}

class DataManager: DataManagerProtocol {
    
    private let provider = MoyaProvider<GiphyAPI>()
    
    static let shared = DataManager()
    
    private init() { }
    
    func getGifs(_ searchText: String, limit: Int = 30, offset: Int = 0) -> Observable<[GifImage]> {
        return provider.rx.request(.search(text: searchText, limit: limit, offset: offset))
            .asObservable()
            .map([GifImage].self, atKeyPath: "data", using: JSONDecoder(), failsOnEmptyData: false)
            .catchErrorJustReturn([])
    }
    
}
