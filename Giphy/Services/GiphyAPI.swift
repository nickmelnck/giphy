//
//  GiphyAPI.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import Moya

enum GiphyAPI {
    
    case search(text: String, limit: Int, offset: Int)
    
}

extension GiphyAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://api.giphy.com")!
    }
    
    var path: String {
        switch self {
        case .search: return "/v1/gifs/search"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .search(text: let text, limit: let limit, offset: let offset):
            return .requestParameters(parameters: ["q": text, "limit": limit, "offset": offset], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["api_key": "ZbykVW2l2IFOjGrTVGFY5Q8T2FCsOf2b"]
    }
}
