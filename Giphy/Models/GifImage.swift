//
//  GifImage.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation

struct GifImage: Decodable {
    
    let id: String
    let title: String
    let url: String
    
    let images: Images
    
}

struct Images: Decodable {
    
    let original: GifFile
    let fixedWidth: GifFile
  
    enum CodingKeys: String, CodingKey {
        case original
        case fixedWidth = "fixed_width"
    }

}

struct GifFile: Decodable {
    
    let width: Int
    let height: Int
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case width
        case height
        case url
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let widthString = try container.decode(String.self, forKey: .width)
        if let widthInt = Int(widthString) {
            self.width = widthInt
        } else {
            throw APIError.invalidData
        }

        let heightString = try container.decode(String.self, forKey: .height)
        if let heightInt = Int(heightString) {
            self.height = heightInt
        } else {
            throw APIError.invalidData
        }

        self.url = try container.decode(String.self, forKey: .url)
    }

    
}
