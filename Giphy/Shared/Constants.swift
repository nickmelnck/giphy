//
//  Constants.swift
//
//  Created by Nick Melnick on 18.02.2019.
//  Copyright © 2019 Nick Melnick. All rights reserved.
//

import Foundation
import UIKit

enum Constants {
    enum UI {
        static let mainColor = UIColor(hex: "222736")
        static let btnColor = UIColor(hex: "33A85A")
        static let lblText = UIColor(hex: "D5D5E8")
        static let lblParam = UIColor(hex: "1A72FF")
    }

    enum Params: String {
        case isFirstLaunch = "isFirstLaunch"
        case permissionsRequest = "PermissionsRequest"
        
        func getValue() -> Any? {
            return UserDefaults.groups.value(forKey: self.rawValue)
        }
        
        func setValue(_ value: Any) {
            UserDefaults.groups.set(value, forKey: self.rawValue)
            UserDefaults.groups.synchronize()
        }
        
        func removeValue() {
            UserDefaults.groups.removeObject(forKey: self.rawValue)
            UserDefaults.groups.synchronize()
        }
        
    }

    static var appVersion: String {
        let appVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let appBuildString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        let appNameString = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as! String
        let versionString = "\(appNameString) v.\(appVersionString) (\(appBuildString))"
        return versionString
    }
    
}
