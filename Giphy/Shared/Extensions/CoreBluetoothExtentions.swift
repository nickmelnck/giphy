//
//  CoreBluetoothExtentions.swift
//  Zalm
//
//  Created by Nick Melnick on 15.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import CoreBluetooth

extension CBPeripheralState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .connected: return "Connected"
        case .connecting: return "Connecting"
        case .disconnected: return "Disconnected"
        case .disconnecting: return "Disconnecting"
        @unknown default:
            fatalError("Unknown peripheral state")
        }
    }
}

