//
//  UserDefaultsExtension.swift
//
//  Created by Nick Melnick on 18.02.2019.
//  Copyright © 2019 Nick Melnick. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static let groups: UserDefaults = {
        guard let identifier = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String else {
            return UserDefaults.standard
        }
        return UserDefaults(suiteName: "group." + identifier) ?? UserDefaults.standard
    }()
    
}
