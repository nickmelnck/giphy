//
//  TimeIntervalExtension.swift
//  Genesis
//
//  Created by Nick Melnick on 09.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    var asDurationString: String? {
        let df = DateComponentsFormatter()
        df.unitsStyle = .brief
        df.allowedUnits = [.minute, .hour]
        df.zeroFormattingBehavior = .dropAll
        df.maximumUnitCount = 2
        return df.string(from: self)
    }
    
    var asTimeString: String {
        let df = DateFormatter()
        df.dateStyle = .none
        df.timeStyle = .short
        return df.string(from: Date(timeIntervalSince1970: self))
    }
    
}
