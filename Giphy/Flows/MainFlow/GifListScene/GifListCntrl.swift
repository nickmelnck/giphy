//
//  GifListCntrl.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GifListCntrl: UIViewController {
    
    fileprivate let disposeBag = DisposeBag()
    
    var viewModel: GifListViewModel!
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerNib(GifImageCell.self)
        configureUI()
        setupRx()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureUI()
    }
    
    func configureUI() {
        let flow = UICollectionViewFlowLayout()
        let width = (self.collectionView.frame.width / 2) - 1
        flow.itemSize = CGSize(width: width, height: width)
        flow.minimumInteritemSpacing = 1
        flow.minimumLineSpacing = 1
        self.collectionView.collectionViewLayout = flow
    }
    
    private func setupRx() {
        viewModel.images.asDriver()
            .drive(onNext: { [unowned self] (_) in
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)

        searchBar.rx.text
            .orEmpty
            .debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
        
        self.collectionView.rx.itemSelected
            .subscribe(onNext: { (indexPath) in
                self.viewModel.showGifAt(indexPath: indexPath)
            })
            .disposed(by: disposeBag)
        
    }
    
}

extension GifListCntrl: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.images.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as GifImageCell
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let gifCell = cell as? GifImageCell {
            let gif = self.viewModel.images.value[indexPath.item]
            gifCell.set(image: gif)
        }
        if indexPath.item == viewModel.images.value.count - 1 {
            viewModel.getNextImages()
        }
    }
    
}

extension GifListCntrl: StoryboardProtocol {
    
    static var storyboardName: String {
        return "GifListFlow"
    }
    
}
