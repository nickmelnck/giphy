//
//  GifImageCell.swift
//  Giphy
//
//  Created by Nick Melnick on 26.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit
import GiphyCoreSDK
import GiphyUISDK

class GifImageCell: UICollectionViewCell, RegisterCellProtocol {

    var gifView: GPHMediaView!
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var lblTitle: UILabel!
    
    private var cancellable: Operation?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .red
        gifView = GPHMediaView()
        contentView.insertSubview(gifView, at: 0)
        gifView.translatesAutoresizingMaskIntoConstraints = false
        gifView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        gifView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        gifView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        gifView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cancellable?.cancel()
        gifView.cancelLoading()
        gifView.media = nil
    }
    
    func set(image: GifImage) {
        lblTitle.text = image.title
        gifView.loadAsset(at: image.images.fixedWidth.url)
    }

}
