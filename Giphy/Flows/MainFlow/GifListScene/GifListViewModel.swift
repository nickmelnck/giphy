//
//  GifListViewModel.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation
import GiphyCoreSDK
import RxSwift
import RxCocoa

class GifListViewModel {
    
    private let disposeBag = DisposeBag()
    
    private unowned var coordinator: GifListCoordinator
    
    private let dataManager: DataManagerProtocol
    
    init(coordinator: GifListCoordinator, dataManager: DataManagerProtocol) {
        self.coordinator = coordinator
        self.dataManager = dataManager
        setupRx()
    }
    
    private func setupRx() {
        self.searchText.distinctUntilChanged()
            .subscribe(onNext: { [unowned self] (_) in
                self.offset = 0
            })
        .disposed(by: disposeBag)

        searchText
            .filter{ !$0.isEmpty }
            .debug("Get start")
            .flatMapLatest { [weak self] (text) -> Observable<[GifImage]> in
                guard let `self` = self else { return Observable.just([GifImage]()) }
                return self.dataManager.getGifs(text, limit: self.limit, offset: 0)
            }
            .catchErrorJustReturn([])
            .bind(to: images)
            .disposed(by: disposeBag)
    }
    
    let images = BehaviorRelay<[GifImage]>(value: [])
    
    let searchText = BehaviorRelay<String>(value: "")
    
    private var offset: Int = 0 {
        didSet {
            print("Offset: \(offset)")
        }
    }
    
    private var limit: Int = 30
    
    func getNextImages() {
        self.offset = images.value.count
        searchText
            .debug("Get Next")
            .flatMapLatest { [weak self] (text) -> Observable<[GifImage]> in
                guard let `self` = self else { return Observable.just([GifImage]()) }
                return self.dataManager.getGifs(text, limit: self.limit, offset: self.offset)
            }
            .catchErrorJustReturn([])
            .take(1)
            .subscribe(
                onNext: { [weak self] (list) in
                    guard let `self` = self else { return }
                    self.images.accept(self.images.value + list)
                }
            )
            .disposed(by: disposeBag)
    }
    
    func showGifAt(indexPath: IndexPath) {
        let gifImage = self.images.value[indexPath.item]
        self.coordinator.showGifView(gifImage: gifImage)
    }
    
}
