//
//  GifListCoordinator.swift
//  Giphy
//
//  Created by Nick Melnick on 25.05.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

class GifListCoordinator: BaseCoordinator {
    
    private var viewModel: GifListViewModel!
    
    override func start() {
        let viewController = GifListCntrl.instantiateViewController()
        self.viewModel = GifListViewModel(coordinator: self, dataManager: DataManager.shared)
        viewController.viewModel = self.viewModel
        self.navigationController.viewControllers = [viewController]
    }
    
    func showGifView(gifImage: GifImage) {
        let gifViewCoordinator = GifViewCoordinator(gifImage: gifImage)
        gifViewCoordinator.parentCoordinator = self
        gifViewCoordinator.navigationController = self.navigationController
        self.start(coordinator: gifViewCoordinator)
    }
    
}
