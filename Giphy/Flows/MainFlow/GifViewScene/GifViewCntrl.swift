//
//  GifViewCntrl.swift
//  Giphy
//
//  Created by Nick Melnick on 04.06.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit
import GiphyCoreSDK
import GiphyUISDK

class GifViewCntrl: UIViewController {
    
    var viewModel: GifViewViewModel!
    
    private let gifView = PlayingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.insertSubview(gifView, at: 0)
        gifView.translatesAutoresizingMaskIntoConstraints = false
        gifView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        gifView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        gifView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        gifView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.gifView.set(image: viewModel.gifImage)
    }
    
}

extension GifViewCntrl: StoryboardProtocol {
    
    static var storyboardName: String {
        return "GifListFlow"
    }
    
}
