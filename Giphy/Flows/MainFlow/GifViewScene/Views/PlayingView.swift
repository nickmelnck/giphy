//
//  PlayingView.swift
//  Giphy
//
//  Created by Nick Melnick on 04.06.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit
import GiphyCoreSDK
import GiphyUISDK

final class PlayingView: UIScrollView, UIScrollViewDelegate {
    
    private var playView = GPHMediaView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        configure()
    }
    
    private func configure() {
        self.addSubview(playView)
        self.delegate = self
        playView.isUserInteractionEnabled = false
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    public func set(image: GifImage) {
        let size = CGSize(width: image.images.original.width, height: image.images.original.height)
        playView.frame = CGRect(origin: .zero, size: size)
        configurateFor(size: size)
        centerImage()
        playView.loadAsset(at: image.images.original.url)
    }
    
    // MARK: - ScrollView Zooming menagement
    internal func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return playView
    }

    internal func scrollViewDidZoom(_ scrollView: UIScrollView) {
        self.centerImage()
    }
    
    // MARK: - View Layout management
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setCurrentMaxMinZoomScale()
        self.zoomScale = self.minimumZoomScale
        centerImage()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.centerImage()
    }
    
    private func centerImage() {
        let boundsSize = self.bounds.size
        var frameToCenter = playView.frame
        
        if frameToCenter.size.width < boundsSize.width {
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2
        } else {
            frameToCenter.origin.x = 0
        }
        
        if frameToCenter.size.height < boundsSize.height {
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2
        } else {
            frameToCenter.origin.y = 0
        }
        
        playView.frame = frameToCenter
    }
    
    private func setCurrentMaxMinZoomScale() {
        let boundsSize = self.bounds.size
        let imageSize = playView.bounds.size
        
        let xScale = boundsSize.width / imageSize.width
        let yScale = boundsSize.height / imageSize.height

        let minScale = min(xScale, yScale)
        let maxScale = minScale * 6.0

        self.minimumZoomScale = minScale
        self.maximumZoomScale = maxScale
    }
    
    private func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        let bounds = self.bounds
        
        zoomRect.size.width = bounds.size.width / scale
        zoomRect.size.height = bounds.size.height / scale
        
        zoomRect.origin.x = max(0, (center.x - (zoomRect.size.width / 2)))
        zoomRect.origin.y = max(0, (center.y - (zoomRect.size.height / 2)))
        return zoomRect
    }
    
    func zoom(point: CGPoint, animated: Bool) {
        let currectScale = self.zoomScale
        let minScale = self.minimumZoomScale
        let maxScale = self.maximumZoomScale
        
        if (minScale == maxScale && minScale > 1) {
            return
        }
        
        let toScale = maxScale
        let finalScale = (currectScale == minScale) ? toScale : minScale
        let zoomRect = self.zoomRect(scale: finalScale, center: point)
        self.zoom(to: zoomRect, animated: animated)
    }
    
    private func configurateFor(size: CGSize) {
        if !self.playView.frame.size.equalTo(size) {
            self.playView.transform = .identity
            self.playView.frame = CGRect(origin: .zero, size: size)
        }
        
        self.contentSize = size
        
        setCurrentMaxMinZoomScale()
        self.zoomScale = self.minimumZoomScale
    }
    
}

