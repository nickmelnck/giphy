//
//  GifViewViewModel.swift
//  Giphy
//
//  Created by Nick Melnick on 04.06.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import Foundation

class GifViewViewModel {
    
    private unowned let coordinator: GifViewCoordinator
    
    let gifImage: GifImage
    
    init(coordinator: GifViewCoordinator, gifImage: GifImage) {
        self.gifImage = gifImage
        self.coordinator = coordinator
    }
    
}
