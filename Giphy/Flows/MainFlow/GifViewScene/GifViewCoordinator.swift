//
//  GifViewCoordinator.swift
//  Giphy
//
//  Created by Nick Melnick on 04.06.2020.
//  Copyright © 2020 Nick Melnick. All rights reserved.
//

import UIKit

final class GifViewCoordinator: BaseCoordinator {
    
    private var viewModel: GifViewViewModel!
    
    convenience init(gifImage: GifImage) {
        self.init()
        self.viewModel = GifViewViewModel(coordinator: self, gifImage: gifImage)
    }
    
    override func start() {
        let viewController = GifViewCntrl.instantiateViewController()
        viewController.viewModel = self.viewModel
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
}
